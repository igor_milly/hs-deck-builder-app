import { CardSet, CardType } from './app/models/entity.models';

/** Provide specified card types only */
export const cardTypes = [CardType.HERO, CardType.MINION, CardType.SPELL];

/** Provide specified card sets only */
export const cardSets = [CardSet.CORE, CardSet.EXPERT1, CardSet.HOF];

/** Provide collectible cards only */
export const collectibleOnly = true;
