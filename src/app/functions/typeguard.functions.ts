/**
 * Assert that a value is not `undefined` and redefine its type.
 */
export function defined<T>(value: T | undefined): value is T {
    return value !== undefined;
}

/**
 * Assert that a value is not `null` and redefine its type.
 */
export function nonNull<T>(value: T | null): value is T {
    return value !== null;
}

/**
 * Assert that a value is not `undefined` or `null` and redefine its type.
 */
export function exists<T>(value: T | null | undefined): value is T {
    return value != null;
}

/**
 * Assert that a value is a string using `typeof` check and redefine its type.
 */
export function isString(value: unknown): value is string {
    return typeof value === 'string';
}

/**
 * Assert that a value is a number using `typeof` check and redefine its type.
 */
export function isNumber(value: unknown): value is number {
    return typeof value === 'number';
}

/**
 * Assert that a value is a boolean using `typeof` check and redefine its type.
 */
export function isBoolean(value: unknown): value is boolean {
    return typeof value === 'boolean';
}

/**
 * Assert that a value is `true` using `===` check and redefine its type.
 */
export function isTrue(value: unknown): value is true {
    return value === true;
}

/**
 * Assert that a value is `false` using `===` check and redefine its type.
 */
export function isFalse(value: unknown): value is false {
    return value === false;
}

/**
 * Assert that a value is not `undefined` or `null` or an empty `ArrayLike` object such as `string` and redefine its type.
 */
export function nonEmpty<T extends ArrayLike<any>>(value?: T | null): value is T {
    return value != null && value.length != null && value.length !== 0;
}
