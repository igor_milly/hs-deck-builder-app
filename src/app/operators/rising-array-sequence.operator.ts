import { Observable, of, SchedulerLike } from 'rxjs';

/**
 * Create an incrementally rising sequence of array items from an array.
 *
 * The first item starts empty, the second item contains a number of elements as specified by the `increment` parameter and so on, until the limit is reached.
 *
 * The last emitted item is the full array.
 *
 * @param array reference array of elements
 * @param increment number of array elements to incrementally add to each emitted item
 * @param limit maximum number of elements before the full array is emitted
 * @param scheduler rxjs scheduler
 */
export function risingArraySequence<T>(array: T[], increment: number, limit: number, scheduler: SchedulerLike): Observable<T[]> {
    const increments = [];

    // TODO this loading mechanics should be removed
    for (let i = 0; i <= limit; i = i + increment) {
        increments.push(array.slice(0, i));
    }

    return of(...increments, array, scheduler);
}
