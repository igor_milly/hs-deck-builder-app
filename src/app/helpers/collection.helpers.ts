import { ComponentFactoryResolver, ComponentRef, Injector } from '@angular/core';
import { Card, CardType, Data } from '../models/entity.models';
import { MinionComponent } from '../modules/entity/components/minion/minion.component';
import { SpellComponent } from '../modules/entity/components/spell/spell.component';

export function createCollectionComponents(resolver: ComponentFactoryResolver, injector: Injector, data: Data<Card>[]): ComponentRef<MinionComponent | SpellComponent>[] {
    return data.map(c => {
        switch (c.entity.type) {
            case CardType.MINION:
                const minionRef = resolver.resolveComponentFactory(MinionComponent).create(injector);
                minionRef.instance.minion = c;
                return minionRef;
            case CardType.SPELL:
                const spellRef = resolver.resolveComponentFactory(SpellComponent).create(injector);
                spellRef.instance.spell = c;
                return spellRef;
            default:
        }
    });
}
