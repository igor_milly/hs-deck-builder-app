export function getStyleUrl(url: string): string;

export function getStyleUrl(url: string | undefined): string | undefined;

export function getStyleUrl(url: string | undefined): string | undefined {
    return url ? `url(${url})` : undefined;
}
