import { EntityImages } from 'src/app/models/data.models';
import { Card, CardRarity, CardType, Data, Entity, Hero, Minion, Spell } from 'src/app/models/entity.models';

export function joinEntityData<E extends Entity>(entity?: E, images?: EntityImages): Data<E> | undefined {
    if (!entity || !images) {
        return undefined;
    }
    return { entity, images };
}

export function isHero(entity?: Entity): entity is Hero {
    if (!entity) {
        return false;
    }
    return entity.type === CardType.HERO;
}

export function isCard(entity?: Entity): entity is Card {
    if (!entity) {
        return false;
    }
    return entity.type === CardType.MINION || entity.type === CardType.SPELL;
}

export function isMinion(entity?: Entity): entity is Minion {
    if (!entity) {
        return false;
    }
    return entity.type === CardType.MINION;
}

export function isSpell(entity?: Entity): entity is Spell {
    if (!entity) {
        return false;
    }
    return entity.type === CardType.SPELL;
}

export function entityComparer<T extends Entity>(a: T, b: T): number {
    const costDiff = compareNums((a as any).cost, (b as any).cost);
    if (costDiff !== 0) {
        return costDiff;
    }
    const classDiff = compareStrings(a.cardClass, b.cardClass);
    if (classDiff !== 0) {
        return classDiff;
    }
    const rarityDiff = compareCardRarity(a.rarity, b.rarity);
    if (rarityDiff !== 0) {
        return rarityDiff;
    }
    return compareStrings(a.name, b.name);
}

function compareNums(a: number = Number.MIN_SAFE_INTEGER, b: number = Number.MIN_SAFE_INTEGER): number {
    return a - b;
}

function compareStrings(a: string = 'zzz', b: string = 'zzz'): number {
    return a.localeCompare(b);
}

function compareCardRarity(a: string = CardRarity.FREE, b: string = CardRarity.FREE): number {
    return getRarityRank(a) - getRarityRank(b);
}

function getRarityRank(rarity: string): number {
    switch (rarity) {
        case CardRarity.FREE:
            return 0;
        case CardRarity.COMMON:
            return 0;
        case CardRarity.RARE:
            return 1;
        case CardRarity.EPIC:
            return 2;
        case CardRarity.LEGENDARY:
            return 3;
        default:
            return 0;
    }
}
