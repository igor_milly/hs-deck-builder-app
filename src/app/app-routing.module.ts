import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { CollectionPageComponent } from './components/collection-page/collection-page.component';
import { DeckPageComponent } from './components/deck-page/deck-page.component';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'welcome' },
    { path: 'welcome', component: WelcomePageComponent },
    { path: 'deck-builder', component: DeckPageComponent },
    { path: 'collection', component: CollectionPageComponent },
    { path: '**', redirectTo: 'welcome' },
];

if (!environment.production) {
    // tslint:disable-next-line: no-array-mutation
    routes.splice(routes.length - 1, 0, { path: 'dev', loadChildren: () => import('./modules/dev/dev.module').then(m => m.DevModule) });
}

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule],
})
export class AppRoutingModule {}
