import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CardCrystals, EntityIcons, EntityImages, EntityTemplates } from '../models/data.models';
import { Entity } from '../models/entity.models';

@Injectable({
    providedIn: 'root',
})
export class DataService {
    readonly baseUrl = '/assets/data';

    constructor(private readonly http: HttpClient) {}

    loadEntities(options: { collectibleOnly: boolean; locale?: string }): Observable<Entity[]> {
        const url = `${this.baseUrl}/hearthstonejson/v1/31353/${options.locale || 'enUS'}/cards${options.collectibleOnly ? '.collectible' : ''}.json`;

        return this.http.get<Entity[]>(url);
    }

    loadTemplates(): Observable<EntityTemplates> {
        const url = `${this.baseUrl}/wowhead/custom/card-template-data.json`;

        return this.http.get<EntityTemplates>(url);
    }

    loadImages(): Observable<EntityImages[]> {
        const url = `${this.baseUrl}/hearthstonejson/mined/card-image-data.json`;

        return this.http.get<EntityImages[]>(url);
    }

    loadCrystals(): Observable<CardCrystals[]> {
        const url = `${this.baseUrl}/wowhead/custom/card-crystal-data.json`;

        return this.http.get<CardCrystals[]>(url);
    }

    loadIcons(): Observable<EntityIcons> {
        const url = `${this.baseUrl}/wowhead/custom/card-icon-data.json`;

        return this.http.get<EntityIcons>(url);
    }
}
