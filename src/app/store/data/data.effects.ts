import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import * as DataActions from './data.actions';

@Injectable()
export class DataEffects {
    @Effect() readonly load$: Observable<Action> = this.actions.pipe(
        ofType(DataActions.loadData),
        concatMap(_ =>
            forkJoin([this.dataService.loadEntities({ collectibleOnly: true }), this.dataService.loadCrystals(), this.dataService.loadIcons(), this.dataService.loadImages(), this.dataService.loadTemplates()]).pipe(
                map(([entities, crystals, icons, images, templates]) => DataActions.loadDataSuccess({ entities, crystals, icons, images, templates })),
                catchError((err?: Partial<HttpErrorResponse>) => {
                    switch (err?.status) {
                        default:
                            return of(DataActions.loadDataError());
                    }
                }),
            ),
        ),
    );

    constructor(private readonly actions: Actions, private readonly dataService: DataService) {}

    ngrxOnInitEffects(): Action {
        return DataActions.loadData();
    }
}
