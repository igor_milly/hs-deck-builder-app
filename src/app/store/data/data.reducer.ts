import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { CardCrystals, CardTemplates, CommonTemplates, EntityIcons, EntityImages } from '../../models/data.models';
import { Entity } from '../../models/entity.models';
import * as DataActions from './data.actions';

export const dataFeatureKey = 'data';

export interface State {
    entities: EntityState<Entity>;
    crystals: EntityState<CardCrystals>;
    icons?: EntityIcons;
    images: EntityState<EntityImages>;
    templates: EntityState<CardTemplates> & { common?: CommonTemplates };
    loadPending: boolean;
    loadError?: boolean;
}

const crystalAdapter = createEntityAdapter<CardCrystals>({
    selectId: ({ cardClass }) => cardClass,
});

const entityAdapter = createEntityAdapter<Entity>({
    selectId: ({ id }) => id,
});

const imageAdapter = createEntityAdapter<EntityImages>({
    selectId: ({ cardId }) => cardId,
});

const templateAdapter = createEntityAdapter<CardTemplates>({
    selectId: ({ cardClass }) => cardClass,
});

const initialState: State = {
    crystals: crystalAdapter.getInitialState(),
    entities: entityAdapter.getInitialState(),
    images: imageAdapter.getInitialState(),
    templates: templateAdapter.getInitialState(),
    loadPending: false,
};

export const reducer = createReducer(
    initialState,
    on(DataActions.loadData, state => ({
        ...state,
        loadPending: true,
        loadError: undefined,
    })),
    on(DataActions.loadDataSuccess, (state, { crystals, entities, icons, images, templates }) => ({
        ...state,
        crystals: crystalAdapter.upsertMany(crystals, state.crystals),
        entities: entityAdapter.upsertMany(entities, state.entities),
        icons,
        images: imageAdapter.upsertMany(images, state.images),
        templates: templateAdapter.upsertMany(templates.cards, { ...state.templates, common: templates.common }),
        loadPending: false,
        loadError: undefined,
    })),
    on(DataActions.loadDataError, state => ({
        ...state,
        loadPending: false,
        loadError: true,
    })),
);

export const crystalSelectors = crystalAdapter.getSelectors();

export const entitySelectors = entityAdapter.getSelectors();

export const imageSelectors = imageAdapter.getSelectors();

export const templateSelectors = templateAdapter.getSelectors();
