import { createAction, props } from '@ngrx/store';
import { CardCrystals, EntityIcons, EntityImages, EntityTemplates } from 'src/app/models/data.models';
import { Entity } from 'src/app/models/entity.models';

export const loadData = createAction('[Data] load');
export const loadDataSuccess = createAction('[Data] load SUCCESS', props<{ entities: Entity[]; crystals: CardCrystals[]; icons: EntityIcons; images: EntityImages[]; templates: EntityTemplates }>());
export const loadDataError = createAction('[Data] load ERROR');
