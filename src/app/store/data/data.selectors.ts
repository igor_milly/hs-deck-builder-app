import { createFeatureSelector, createSelector } from '@ngrx/store';
import { defined } from '../../functions/typeguard.functions';
import { joinEntityData } from '../../helpers/entity.helpers';
import { CardClass } from '../../models/entity.models';
import * as fromData from './data.reducer';

export const selectDataState = createFeatureSelector<fromData.State>(fromData.dataFeatureKey);

export const selectDataImageState = createSelector(selectDataState, ({ images }) => images);
export const getDataImageIds = createSelector(selectDataImageState, fromData.imageSelectors.selectIds);
export const getDataImageEntities = createSelector(selectDataImageState, fromData.imageSelectors.selectEntities);
export const getDataImages = createSelector(selectDataImageState, fromData.imageSelectors.selectAll);
export const getDataImagesTotal = createSelector(selectDataImageState, fromData.imageSelectors.selectTotal);
export const getDataImageByEntityId = createSelector(getDataImageEntities, data => (entityId: string) => data[entityId]);

export const selectDataTemplateState = createSelector(selectDataState, ({ templates }) => templates);
export const getDataTemplateIds = createSelector(selectDataTemplateState, fromData.templateSelectors.selectIds);
export const getDataTemplateEntities = createSelector(selectDataTemplateState, fromData.templateSelectors.selectEntities);
export const getDataTemplates = createSelector(selectDataTemplateState, fromData.templateSelectors.selectAll);
export const getDataTemplatesTotal = createSelector(selectDataTemplateState, fromData.templateSelectors.selectTotal);
export const getDataTemplateByCardClass = createSelector(getDataTemplateEntities, data => (cardClass: CardClass) => data[cardClass]);
export const getDataTemplateCommon = createSelector(getDataTemplateEntities, ({ common }) => common);

export const selectDataCrystalState = createSelector(selectDataState, ({ crystals }) => crystals);
export const getDataCrystalIds = createSelector(selectDataCrystalState, fromData.crystalSelectors.selectIds);
export const getDataCrystalEntities = createSelector(selectDataCrystalState, fromData.crystalSelectors.selectEntities);
export const getDataCrystals = createSelector(selectDataCrystalState, fromData.crystalSelectors.selectAll);
export const getDataCrystalsTotal = createSelector(selectDataCrystalState, fromData.crystalSelectors.selectTotal);
export const getDataCrystalByCardClass = createSelector(getDataCrystalEntities, data => (cardClass: CardClass) => data[cardClass]);

export const selectDataEntityState = createSelector(selectDataState, ({ entities }) => entities);
export const getDataEntityIds = createSelector(selectDataEntityState, fromData.entitySelectors.selectIds);
export const getDataEntityEntities = createSelector(selectDataEntityState, fromData.entitySelectors.selectEntities);
export const getDataEntities = createSelector(selectDataEntityState, fromData.entitySelectors.selectAll);
export const getDataEntitiesTotal = createSelector(selectDataEntityState, fromData.entitySelectors.selectTotal);
export const getDataByEntityId = createSelector(getDataEntityEntities, getDataImageByEntityId, (entities, getImages) => (entityId: string) => joinEntityData(entities[entityId], getImages(entityId)));

export const isDataLoadPending = createSelector(selectDataState, ({ loadPending }) => loadPending);
export const getDataLoadError = createSelector(selectDataState, ({ loadError }) => loadError);
export const hasDataLoadError = createSelector(getDataLoadError, defined);
