import { createReducer, on } from '@ngrx/store';
import { Deck } from '../../models/deck.models';
import { addCardToDeckSuccess, removeCardFromDeck, removeCardsFromDeck, saveDeck, saveDeckFail, saveDeckSuccess, selectDeckHero } from './deck.actions';
import { createDeckAdapter } from './deck.adapter';

export const deckFeatureKey = 'deck';

export interface State {
    deck: Deck;
    saving: boolean;
    saved: boolean;
}

const adapter = createDeckAdapter();

// TODO remove testing defaults
const initialState: State = {
    deck: {
        // TODO remove test values
        heroId: 'HERO_09',
        cardIdCounts: [
            { id: 'EX1_012', count: 1 },
            { id: 'EX1_001', count: 2 },
            { id: 'CS2_181', count: 2 },
            { id: 'CS1_129', count: 1 },
            { id: 'EX1_091', count: 2 },
            { id: 'CS2_003', count: 2 },
            { id: 'EX1_625', count: 2 },
            { id: 'CS1_113', count: 1 },
            { id: 'EX1_350', count: 1 },
            { id: 'EX1_561', count: 1 },
            { id: 'EX1_572', count: 1 },
        ],
    },
    saving: false,
    saved: true,
};

export const reducer = createReducer(
    initialState,
    on(selectDeckHero, (state, { id }) => ({ ...state, heroId: id, saved: false })),
    on(addCardToDeckSuccess, (state, { card }) => ({ ...state, deck: adapter.addCard(card.id, state.deck), saved: false })),
    on(removeCardFromDeck, (state, { id }) => ({ ...state, deck: adapter.removeCard(id, state.deck), saved: false })),
    on(removeCardsFromDeck, state => ({ ...state, deck: adapter.removeCards(state.deck), saved: false })),
    on(saveDeck, state => ({ ...state, saving: true })),
    on(saveDeckSuccess, state => ({ ...state, saving: false, saved: true })),
    on(saveDeckFail, state => ({ ...state, saving: false })),
);

export const { getHeroId, getCardIdCounts } = adapter.getSelectors();
