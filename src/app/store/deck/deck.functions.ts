import { CardType, Data, Entity } from 'src/app/models/entity.models';
import { CardCount, IdCount } from '../../models/deck.models';

export function toCardCounts(cardIdCounts: IdCount[], getEntityData: (entityId: string) => Data<Entity> | undefined): CardCount[] {
    return cardIdCounts.map(({ id, count }) => ({ card: getEntityData(id), count })).filter(isCardCount);
}

function isCardCount(item: { card: Data<Entity> | undefined; count: number }): item is CardCount {
    return item.card !== undefined && (item.card.entity.type === CardType.MINION || item.card.entity.type === CardType.SPELL);
}
