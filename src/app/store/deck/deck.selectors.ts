import { createFeatureSelector, createSelector } from '@ngrx/store';
import { getDataByEntityId } from '../../store/data/data.selectors';
import { RootState } from '../app.store';
import { toCardCounts } from './deck.functions';
import * as fromDeck from './deck.reducer';

export const getDeckState = createFeatureSelector<RootState, fromDeck.State>(fromDeck.deckFeatureKey);

export const getDeck = createSelector(getDeckState, ({ deck }) => deck);

export const getDeckHeroId = createSelector(getDeck, fromDeck.getHeroId);

export const getDeckHero = createSelector(getDeckHeroId, getDataByEntityId, (id, getData) => (id !== null ? getData(id) : null));

export const getDeckHeroClass = createSelector(getDeckHero, hero => (hero !== null ? hero?.entity.cardClass : null));

export const getDeckCardIdCounts = createSelector(getDeck, fromDeck.getCardIdCounts);

export const getDeckCardCounts = createSelector(getDeckCardIdCounts, getDataByEntityId, toCardCounts);

export const getDeckCardsTotal = createSelector(getDeckCardIdCounts, counts => counts.reduce((acc, c) => acc + c.count, 0));

export const hasDeckCards = createSelector(getDeckCardIdCounts, ({ length }) => !!length);

export const isDeckSaved = createSelector(getDeckState, ({ saved }) => saved);

export const isDeckSaving = createSelector(getDeckState, ({ saving }) => saving);
