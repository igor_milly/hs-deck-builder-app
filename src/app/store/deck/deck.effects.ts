import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { map, mapTo, withLatestFrom } from 'rxjs/operators';
import { CardRarity } from 'src/app/models/entity.models';
import { AppState } from '../app.store';
import { getDeck } from '../deck/deck.selectors';
import { addCardToDeck, addCardToDeckFail, addCardToDeckSuccess, saveDeck, saveDeckSuccess } from './deck.actions';

const MAX_CARD_COPIES = 2;
const MAX_CARDS = 30;

@Injectable()
export class DeckEffects {
    @Effect() readonly save = this.actions.pipe(ofType(saveDeck), mapTo(saveDeckSuccess()));

    @Effect() readonly add = this.actions.pipe(
        ofType(addCardToDeck),
        map(({ card }) => card),
        withLatestFrom(this.store.pipe(select(getDeck))),
        map(([card, deck]) => {
            const count = deck.cardIdCounts.find(c => c.id === card.id)?.count || 0;

            if (count >= MAX_CARD_COPIES) {
                return addCardToDeckFail({ message: `Only ${MAX_CARD_COPIES} copies of a card per deck allowed.` });
            }

            if (count === 1 && card?.rarity === CardRarity.LEGENDARY) {
                return addCardToDeckFail({ message: `Only 1 legendary card per deck allowed.` });
            }

            const total = deck.cardIdCounts.reduce((acc, c) => acc + c.count, 0);

            if (total >= MAX_CARDS) {
                return addCardToDeckFail({ message: `Only ${MAX_CARDS} cards per deck allowed.` });
            }
            return addCardToDeckSuccess({ card });
        }),
    );

    constructor(private readonly actions: Actions, private readonly store: Store<AppState>) {}
}
