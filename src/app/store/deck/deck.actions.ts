import { createAction, props } from '@ngrx/store';
import { Card } from 'src/app/models/entity.models';

export const selectDeckHero = createAction('[Deck] select hero', props<{ id?: string }>());

export const addCardToDeck = createAction('[Deck] add card', props<{ card: Card }>());
export const addCardToDeckSuccess = createAction('[Deck] add card SUCCESS', props<{ card: Card }>());
export const addCardToDeckFail = createAction('[Deck] add card FAIL', props<{ message: string }>());

export const removeCardFromDeck = createAction('[Deck] remove card', props<{ id: string }>());
export const removeCardsFromDeck = createAction('[Deck] remove cards');

export const saveDeck = createAction('[Deck] save');
export const saveDeckSuccess = createAction('[Deck] save SUCCESS');
export const saveDeckFail = createAction('[Deck] save FAIL');
