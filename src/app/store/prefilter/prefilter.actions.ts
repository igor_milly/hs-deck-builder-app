import { createAction, props } from '@ngrx/store';
import { CardPrefilter } from '../../models/library.models';

export const initCardPrefilter = createAction('[Prefilter] init cards', props<{ items: CardPrefilter[] }>());
export const initHeroPrefilter = createAction('[Prefilter] init heroes', props<{ ids: string[] }>());
