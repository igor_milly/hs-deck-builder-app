import { Card, CardClass, Entity, Hero } from 'src/app/models/entity.models';
import { CardPrefilter } from '../../models/library.models';

export function createCollectibleCardPrefilter(items: Entity[]): CardPrefilter[] {
    const classes: CardClass[] = items
        .map(item => item.cardClass)
        .reduce((acc, c) => {
            if (c && !acc.includes(c)) {
                return [...acc, c];
            }
            return acc;
        }, [] as CardClass[]);

    const collectibleItems = items.filter(item => item.collectible);

    return classes.map(cardClass => {
        const result: CardPrefilter = {
            cardClass,
            ids: collectibleItems
                .filter(item => item.cardClass === cardClass)
                .filter(item => item.type === 'MINION' || item.type === 'SPELL')
                .map(item => item.id),
            minionIdsWithNeutral: collectibleItems
                .filter(item => item.cardClass === cardClass || item.cardClass === 'NEUTRAL')
                .filter(item => item.type === 'MINION')
                .map(item => item.id),
            spellIdsWithNeutral: collectibleItems
                .filter(item => item.cardClass === cardClass || item.cardClass === 'NEUTRAL')
                .filter(item => item.type === 'SPELL')
                .map(item => item.id),
        };
        return result;
    });
}

export function selectCollectibleHeroIds(items: Entity[]): string[] {
    return items
        .filter(item => item.collectible)
        .filter(item => item.type === 'HERO')
        .map(item => item.id);
}

function sortHeroes(a: Hero, b: Hero): number {
    return a.name.localeCompare(b.name);
}

function sortCards(a: Card, b: Card): number {
    return a.name.localeCompare(b.name);
}
