import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CardClass } from '../../models/entity.models';
import { RootState } from '../app.store';
import * as fromPrefilter from './prefilter.reducer';

export const getPrefilterState = createFeatureSelector<RootState, fromPrefilter.State>(fromPrefilter.prefilterFeatureKey);

export const getPrefilterCardData = createSelector(getPrefilterState, fromPrefilter.selectEntities);

export const getPrefilterCardIdsByClass = createSelector(getPrefilterCardData, prefilter => (cardClass: CardClass | null | undefined) => (cardClass ? prefilter[cardClass]?.ids || [] : []));

export const getPrefilterMinionIdsByClassWithNeutral = createSelector(getPrefilterCardData, prefilter => (cardClass: CardClass | null | undefined) => (cardClass ? prefilter[cardClass]?.minionIdsWithNeutral || [] : []));

export const getPrefilterSpellIdsByClassWithNeutral = createSelector(getPrefilterCardData, prefilter => (cardClass: CardClass | null | undefined) => (cardClass ? prefilter[cardClass]?.spellIdsWithNeutral || [] : []));

export const getPrefilterHeroIds = createSelector(getPrefilterState, ({ heroIds }) => heroIds);
