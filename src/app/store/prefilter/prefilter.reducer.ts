import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { CardPrefilter } from '../../models/library.models';
import { initCardPrefilter, initHeroPrefilter } from './prefilter.actions';

export const PAGE_SIZE = 10;

export const prefilterFeatureKey = 'prefilter';

export interface State extends EntityState<CardPrefilter> {
    heroIds: string[];
}

const cardAdapter = createEntityAdapter<CardPrefilter>({
    selectId: item => item.cardClass,
});

const initialState: State = cardAdapter.getInitialState({
    heroIds: [],
});

export const reducer = createReducer(
    initialState,
    on(initCardPrefilter, (state, { items }) => cardAdapter.setAll(items, state)),
    on(initHeroPrefilter, (state, { ids }) => ({ ...state, heroIds: ids })),
);

export const { selectAll, selectEntities, selectIds, selectTotal } = cardAdapter.getSelectors();
