import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { getDataEntities } from 'src/app/store/data/data.selectors';
import { AppState } from '../app.store';
import { initCardPrefilter, initHeroPrefilter } from './prefilter.actions';
import { createCollectibleCardPrefilter, selectCollectibleHeroIds } from './prefilter.internal';

@Injectable()
export class PrefilterEffects {
    @Effect() readonly cardPrefilterInit = this.store.pipe(
        select(getDataEntities),
        map(items => initCardPrefilter({ items: createCollectibleCardPrefilter(items) })), // TODO importing any entity
    );

    @Effect() readonly heroPrefilterInit = this.store.pipe(
        select(getDataEntities),
        map(items => initHeroPrefilter({ ids: selectCollectibleHeroIds(items) })), // TODO importing any entity
    );

    constructor(private readonly store: Store<AppState>) {}
}
