import { BaseRouterStoreState, getSelectors, RouterReducerState } from '@ngrx/router-store';
import { createFeatureSelector } from '@ngrx/store';
import { AppState } from '../app.store';

export const getRouterState = createFeatureSelector<AppState, RouterReducerState<BaseRouterStoreState>>('router');

export const getRouterUrl = getSelectors(getRouterState).selectUrl;

export const getRouterParam = getSelectors(getRouterState).selectRouteParam;

export const getRouterQueryParam = getSelectors(getRouterState).selectQueryParam;

export const getRouterFragment = getSelectors(getRouterState).selectFragment;

export const getRouterData = getSelectors(getRouterState).selectRouteData;
