import { createAction, props } from '@ngrx/store';
import { SelectionFilter } from '../../models/library.models';

export const filterSelectionCards = createAction('[Selection] filter', props<{ filter: SelectionFilter }>());
