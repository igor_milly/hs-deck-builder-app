import { createReducer, on } from '@ngrx/store';
import { SelectionFilter } from '../../models/library.models';
import { filterSelectionCards } from './selection.actions';

export const selectionFeatureKey = 'selection';

export interface State {
    filter: SelectionFilter;
}

const initialState: State = {
    filter: SelectionFilter.CLASS,
};

export const reducer = createReducer(
    initialState,
    on(filterSelectionCards, (state, { filter }) => ({ ...state, filter, page: 1 })),
);
