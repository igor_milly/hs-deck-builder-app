import { createFeatureSelector, createSelector } from '@ngrx/store';
import { exists } from '../../functions/typeguard.functions';
import { CardClass } from '../../models/entity.models';
import { SelectionFilter } from '../../models/library.models';
import { getDataByEntityId } from '../../store/data/data.selectors';
import { RootState } from '../app.store';
import { getDeckHeroClass } from '../deck/deck.selectors';
import { getPrefilterCardIdsByClass, getPrefilterHeroIds, getPrefilterMinionIdsByClassWithNeutral, getPrefilterSpellIdsByClassWithNeutral } from '../prefilter/prefilter.selectors';
import * as fromSelection from './selection.reducer';

export const getSelectionState = createFeatureSelector<RootState, fromSelection.State>(fromSelection.selectionFeatureKey);

export const getSelectionFilter = createSelector(getSelectionState, ({ filter }) => filter);

export const getSelectionHeroes = createSelector(getPrefilterHeroIds, getDataByEntityId, (ids, getData) => ids.map(getData).filter(exists));

export const getSelectionHeroesTotal = createSelector(getSelectionHeroes, ({ length }) => length);

export const getSelectionCardIds = createSelector(
    getSelectionFilter,
    getDeckHeroClass,
    getPrefilterCardIdsByClass,
    getPrefilterMinionIdsByClassWithNeutral,
    getPrefilterSpellIdsByClassWithNeutral,
    (filter, deckHeroClass, getPrefilteredCardIds, getPrefilteredMinionIds, getPrefilteredSpellIds) => {
        switch (filter) {
            case SelectionFilter.CLASS:
                return getPrefilteredCardIds(deckHeroClass);

            case SelectionFilter.NEUTRAL:
                return getPrefilteredCardIds(CardClass.NEUTRAL);

            case SelectionFilter.MINION:
                return getPrefilteredMinionIds(deckHeroClass);

            case SelectionFilter.SPELL:
                return getPrefilteredSpellIds(deckHeroClass);

            default:
                return [];
        }
    },
);

export const getSelectionCards = createSelector(getSelectionCardIds, getDataByEntityId, (ids, getCard) => ids.map(getCard).filter(exists));

export const getSelectionCardsTotal = createSelector(getSelectionCardIds, ({ length }) => length); // caution: based on id count

export const hasSelectionCards = createSelector(getSelectionCardIds, ({ length }) => !!length); // caution: based on id count
