import { MediaChange } from '@angular/flex-layout';
import { createReducer, on } from '@ngrx/store';
import { activateLayoutBreakpoint } from './layout.actions';

export const layoutFeatureKey = 'layout';

export interface State {
    change: MediaChange[];
}

const initialState: State = {
    change: [],
};

export const reducer = createReducer(
    initialState,
    on(activateLayoutBreakpoint, (state, { change }) => ({ ...state, change })),
);
