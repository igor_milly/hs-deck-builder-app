import { MediaChange } from '@angular/flex-layout';
import { createAction, props } from '@ngrx/store';

export const activateLayoutBreakpoint = createAction('[Layout] activate breakpoint', props<{ change: MediaChange[] }>());
