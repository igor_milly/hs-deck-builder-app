import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../app.store';
import * as fromLayout from './Layout.reducer';

export const selectLayoutState = createFeatureSelector<AppState, fromLayout.State>(fromLayout.layoutFeatureKey);

export const getLayoutChange = createSelector(selectLayoutState, ({ change }) => change);
