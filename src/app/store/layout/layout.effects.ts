import { Injectable } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { Effect } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { activateLayoutBreakpoint } from './layout.actions';

@Injectable()
export class LayoutEffects {
    @Effect() readonly breakpointChange$ = this.observer.asObservable().pipe(map(change => activateLayoutBreakpoint({ change })));

    constructor(private readonly observer: MediaObserver) {}
}
