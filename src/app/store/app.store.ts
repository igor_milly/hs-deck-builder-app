import { MinimalRouterStateSnapshot, routerReducer, RouterReducerState, RouterState, StoreRouterConfig } from '@ngrx/router-store';
import { ActionReducerMap, RootStoreConfig } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import * as fromCollection from './collection/collection.reducer';
import { DataEffects } from './data/data.effects';
import * as fromData from './data/data.reducer';
import { DeckEffects } from './deck/deck.effects';
import * as fromDeck from './deck/deck.reducer';
import { LayoutEffects } from './layout/layout.effects';
import * as fromLayout from './layout/layout.reducer';
import { PrefilterEffects } from './prefilter/prefilter.effects';
import * as fromPrefilter from './prefilter/prefilter.reducer';
import * as fromSelection from './selection/selection.reducer';

export interface RootState {
    router: RouterReducerState<MinimalRouterStateSnapshot>;
    [fromData.dataFeatureKey]: fromData.State;
    [fromLayout.layoutFeatureKey]: fromLayout.State;

    [fromCollection.collectionFeatureKey]: fromCollection.State;
    [fromDeck.deckFeatureKey]: fromDeck.State;
    [fromPrefilter.prefilterFeatureKey]: fromPrefilter.State;
    [fromSelection.selectionFeatureKey]: fromSelection.State;
}

export const reducers: ActionReducerMap<RootState> = {
    router: routerReducer,
    [fromData.dataFeatureKey]: fromData.reducer,
    [fromLayout.layoutFeatureKey]: fromLayout.reducer,

    [fromCollection.collectionFeatureKey]: fromCollection.reducer,
    [fromDeck.deckFeatureKey]: fromDeck.reducer,
    [fromPrefilter.prefilterFeatureKey]: fromPrefilter.reducer,
    [fromSelection.selectionFeatureKey]: fromSelection.reducer,
};

export interface AppState extends RootState {}

export const effects = [DataEffects, LayoutEffects, PrefilterEffects, DeckEffects];

export const config: RootStoreConfig<AppState> = {
    metaReducers: !environment.production ? [] : [],
    runtimeChecks: {
        strictActionImmutability: !environment.production,
        strictStateImmutability: !environment.production,
        strictActionSerializability: !environment.production,
        strictStateSerializability: !environment.production,
        strictActionWithinNgZone: !environment.production,
    },
};

export const routerStoreConfig: StoreRouterConfig = {
    routerState: RouterState.Minimal,
};
