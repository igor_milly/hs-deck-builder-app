import { createAction, props } from '@ngrx/store';
import { CardClass } from 'src/app/models/entity.models';

export const filterCollectionCards = createAction('[Collection] filter', props<{ filter: CardClass }>());
