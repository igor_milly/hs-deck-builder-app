import { createReducer, on } from '@ngrx/store';
import { CardClass } from 'src/app/models/entity.models';
import { filterCollectionCards } from './collection.actions';

export const collectionFeatureKey = 'collection';

export interface State {
    filter: CardClass;
}

const initialState: State = {
    filter: CardClass.NEUTRAL,
};

export const reducer = createReducer(
    initialState,
    on(filterCollectionCards, (state, { filter }) => ({ ...state, filter, page: 1 })),
);
