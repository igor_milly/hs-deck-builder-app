import { createFeatureSelector, createSelector } from '@ngrx/store';
import { exists } from '../../functions/typeguard.functions';
import { RootState } from '../app.store';
import { getDataByEntityId } from '../data/data.selectors';
import { getPrefilterCardIdsByClass } from '../prefilter/prefilter.selectors';
import * as fromCollection from './collection.reducer';

export const getCollectionState = createFeatureSelector<RootState, fromCollection.State>(fromCollection.collectionFeatureKey);

export const getCollectionFilter = createSelector(getCollectionState, ({ filter }) => filter);

export const getCollectionCardIds = createSelector(getCollectionFilter, getPrefilterCardIdsByClass, (filter, getPrefilterCardIds) => getPrefilterCardIds(filter));

export const getCollectionCardsTotal = createSelector(getCollectionCardIds, ({ length }) => length); // caution: based on id count

export const hasCollectionCards = createSelector(getCollectionCardIds, ({ length }) => !!length); // caution: based on id count

export const getCollectionCards = createSelector(getCollectionCardIds, getDataByEntityId, (ids, getData) => ids.map(getData).filter(exists));
