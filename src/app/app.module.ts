import { HttpClientModule } from '@angular/common/http';
import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveComponentModule } from '@ngrx/component';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import * as appConfig from '../config';
import { AppRoutingModule } from './app-routing.module';
import { CollectionPageComponent } from './components/collection-page/collection-page.component';
import { DeckPageComponent } from './components/deck-page/deck-page.component';
import { RootComponent } from './components/root/root.component';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';
import { DataOptions } from './models/app.models';
import { CoreModule } from './modules/core/core.module';
import { EntityModule } from './modules/entity/entity.module';
import { SharedModule } from './modules/shared/shared.module';
import { config, effects, reducers, routerStoreConfig } from './store/app.store';

// TODO rename as app config
const dataOptions = new DataOptions(appConfig.collectibleOnly, appConfig.cardSets, appConfig.cardTypes);

@NgModule({
    imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        ReactiveComponentModule,
        StoreModule.forRoot(reducers, config),
        EffectsModule.forRoot(effects),
        StoreRouterConnectingModule.forRoot(routerStoreConfig),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
        FlexLayoutModule.withConfig({}),
        CoreModule,
        EntityModule,
        SharedModule,
        AppRoutingModule,
    ],
    declarations: [RootComponent, WelcomePageComponent, CollectionPageComponent, DeckPageComponent],
    providers: [{ provide: DataOptions, useValue: dataOptions }],
})
export class AppModule implements DoBootstrap {
    ngDoBootstrap(appRef: ApplicationRef): void {
        appRef.bootstrap(RootComponent);
    }
}
