import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { Data, Hero } from 'src/app/models/entity.models';

const heroTemplate = 'assets/img/template/board/standard-hero-template.png';

const boardHeroTemplate = 'assets/img/template/board/board-hero-template.png';

@Component({
    selector: 'hs-hero',
    templateUrl: './hero.component.html',
    styleUrls: ['./hero.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeroComponent {
    @Input() hero!: Data<Hero>;

    readonly templateUrl = heroTemplate;
}
