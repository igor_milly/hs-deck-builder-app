import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { Card, CardRarity, Data } from 'src/app/models/entity.models';

const cardTemplate = 'assets/img/template/selection-card-template.png';

@Component({
    selector: 'hs-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {
    @Input() card!: Data<Card>;
    @Input() count?: number;

    readonly templateUrl = cardTemplate;

    readonly CardRarity = CardRarity;
}
