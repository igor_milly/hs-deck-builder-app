import { CardClass, CardRarity } from 'src/app/store/entity';

const templates: ReadonlyMap<string, string> = new Map([
    [CardClass.DRUID, 'assets/img/template/weapon/druid-weapon-template.png'],
    [CardClass.HUNTER, 'assets/img/template/weapon/hunter-weapon-template.png'],
    [CardClass.MAGE, 'assets/img/template/weapon/mage-weapon-template.png'],
    [CardClass.PALADIN, 'assets/img/template/weapon/paladin-weapon-template.png'],
    [CardClass.PRIEST, 'assets/img/template/weapon/priest-weapon-template.png'],
    [CardClass.ROGUE, 'assets/img/template/weapon/rogue-weapon-template.png'],
    [CardClass.SHAMAN, 'assets/img/template/weapon/shaman-weapon-template.png'],
    [CardClass.WARLOCK, 'assets/img/template/weapon/warlock-weapon-template.png'],
    [CardClass.WARRIOR, 'assets/img/template/weapon/warrior-weapon-template.png'],
    [CardClass.NEUTRAL, 'assets/img/template/weapon/neutral-weapon-template.png'],
]);

const crystals: ReadonlyMap<string, string> = new Map([
    [CardRarity.COMMON, 'assets/img/template/weapon/common-weapon-crystal.png'],
    [CardRarity.RARE, 'assets/img/template/weapon/rare-weapon-crystal.png'],
    [CardRarity.EPIC, 'assets/img/template/weapon/epic-weapon-crystal.png'],
    [CardRarity.LEGENDARY, 'assets/img/template/weapon/legendary-weapon-crystal.png'],
]);
