import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { CardClass, CardRarity, Data, Spell } from 'src/app/models/entity.models';
import { getStyleUrl } from '../../../../helpers/style.helpers';

const defaultSpellTemplate = 'assets/img/template/spell/neutral-spell-template.png';

const templates: ReadonlyMap<CardClass, string> = new Map([
    [CardClass.DRUID, 'assets/img/template/spell/druid-spell-template.png'],
    [CardClass.HUNTER, 'assets/img/template/spell/hunter-spell-template.png'],
    [CardClass.MAGE, 'assets/img/template/spell/mage-spell-template.png'],
    [CardClass.PALADIN, 'assets/img/template/spell/paladin-spell-template.png'],
    [CardClass.PRIEST, 'assets/img/template/spell/priest-spell-template.png'],
    [CardClass.ROGUE, 'assets/img/template/spell/rogue-spell-template.png'],
    [CardClass.SHAMAN, 'assets/img/template/spell/shaman-spell-template.png'],
    [CardClass.WARLOCK, 'assets/img/template/spell/warlock-spell-template.png'],
    [CardClass.WARRIOR, 'assets/img/template/spell/warrior-spell-template.png'],
    [CardClass.NEUTRAL, 'assets/img/template/spell/neutral-spell-template.png'],
]);

const crystals: ReadonlyMap<CardRarity, string> = new Map([
    [CardRarity.COMMON, 'assets/img/template/spell/common-spell-crystal.png'],
    [CardRarity.RARE, 'assets/img/template/spell/rare-spell-crystal.png'],
    [CardRarity.EPIC, 'assets/img/template/spell/epic-spell-crystal.png'],
    [CardRarity.LEGENDARY, 'assets/img/template/spell/legendary-spell-crystal.png'],
]);

@Component({
    selector: 'hs-spell',
    templateUrl: './spell.component.html',
    styleUrls: ['./spell.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SpellComponent {
    @Input() spell!: Data<Spell>;

    get templateUrl(): string | undefined {
        return templates.get(this.spell.entity.cardClass || CardClass.NEUTRAL);
    }

    get rarityCrystalStyleUrl(): string | undefined {
        return this.spell.entity.rarity ? getStyleUrl(crystals.get(this.spell.entity.rarity)) : undefined;
    }
}
