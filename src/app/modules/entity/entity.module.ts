import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CardComponent } from './components/card/card.component';
import { HeroComponent } from './components/hero/hero.component';
import { MinionComponent } from './components/minion/minion.component';
import { SpellComponent } from './components/spell/spell.component';

const EXPORTED_DECLARATIONS = [CardComponent, MinionComponent, SpellComponent, HeroComponent];

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [EXPORTED_DECLARATIONS],
    exports: [EXPORTED_DECLARATIONS],
})
export class EntityModule {}
