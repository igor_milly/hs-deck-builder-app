import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { coreEffects } from './store/core.store';

/**
 * Bundle globally used misc logic, such as component management (e.g. opening messages, closing dialogs, etc...).
 */
@NgModule({
    imports: [EffectsModule.forFeature(coreEffects)],
})
export class CoreModule {}
