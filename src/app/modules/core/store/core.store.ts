import { DialogEffects } from './dialog/dialog.effects';
import { ToastEffects } from './toast/toast.effects';

export const coreEffects = [DialogEffects, ToastEffects];
