import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import * as ToastActions from './toast.actions';

@Injectable()
export class ToastEffects {
    @Effect({ dispatch: false }) readonly showSuccessToast$ = this.actions$.pipe(
        ofType(ToastActions.showSuccessToast),
        tap(({ message, options }) => this.snackbar.open(message, undefined, { duration: 3000, ...options, panelClass: 'success' })),
    );

    @Effect({ dispatch: false }) readonly showWarningToast$ = this.actions$.pipe(
        ofType(ToastActions.showWarningToast),
        tap(({ message, options }) => this.snackbar.open(message, undefined, { duration: 7000, ...options, panelClass: 'warning' })),
    );

    @Effect({ dispatch: false }) readonly showErrorToast$ = this.actions$.pipe(
        ofType(ToastActions.showErrorToast),
        tap(({ message, options }) => this.snackbar.open(message, undefined, { duration: 5000, ...options, panelClass: 'error' })),
    );

    @Effect({ dispatch: false }) readonly showInfoToast$ = this.actions$.pipe(
        ofType(ToastActions.showInfoToast),
        tap(({ message, options }) => this.snackbar.open(message, undefined, { duration: 7000, ...options, panelClass: 'info' })),
    );

    constructor(private readonly actions$: Actions, private readonly snackbar: MatSnackBar) {}
}
