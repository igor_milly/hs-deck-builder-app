import { MatSnackBarConfig } from '@angular/material/snack-bar';
import { createAction, props } from '@ngrx/store';

export const showSuccessToast = createAction('[Toast] show success', props<{ message: string; options?: Pick<MatSnackBarConfig, 'duration'> }>());

export const showWarningToast = createAction('[Toast] show warning', props<{ message: string; options?: Pick<MatSnackBarConfig, 'duration'> }>());

export const showErrorToast = createAction('[Toast] show error', props<{ message: string; options?: Pick<MatSnackBarConfig, 'duration'> }>());

export const showInfoToast = createAction('[Toast] show info', props<{ message: string; options?: Pick<MatSnackBarConfig, 'duration'> }>());
