import { createAction } from '@ngrx/store';

export const closeAllDialogs = createAction('[Dialog] close all');
