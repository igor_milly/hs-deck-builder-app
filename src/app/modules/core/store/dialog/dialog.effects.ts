import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import * as DialogActions from './dialog.actions';

@Injectable()
export class DialogEffects {
    @Effect({ dispatch: false }) readonly closeAll$ = this.actions$.pipe(
        ofType(DialogActions.closeAllDialogs),
        tap(_ => this.dialog.closeAll()),
    );

    constructor(private readonly actions$: Actions, private readonly dialog: MatDialog) {}
}
