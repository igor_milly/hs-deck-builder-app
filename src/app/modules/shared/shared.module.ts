import { CommonModule } from '@angular/common';
import { NgModule, Type } from '@angular/core';
import { EntityButtonComponent } from './components/entity-button/entity-button.component';
import { ButtonSpinnerDirective } from './directives/button-spinner.directive';
import { BootstrapModule } from './modules/bootstrap/bootstrap.module';
import { MaterialModule } from './modules/material/material.module';
import { RemoveXmlTagsPipe } from './pipes/remove-xml-tags.pipe';
import { ResponseErrorPipe } from './pipes/response-error.pipe';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { ValidationErrorPipe } from './pipes/validation-error.pipe';

const EXPORTED_DECLARATIONS: Type<any>[] = [EntityButtonComponent, ButtonSpinnerDirective, ResponseErrorPipe, ValidationErrorPipe, SafeHtmlPipe, RemoveXmlTagsPipe];

@NgModule({
    imports: [CommonModule, BootstrapModule, MaterialModule],
    declarations: [EXPORTED_DECLARATIONS],
    exports: [BootstrapModule, MaterialModule, EXPORTED_DECLARATIONS],
})
export class SharedModule {}
