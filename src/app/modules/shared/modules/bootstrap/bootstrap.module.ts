import { NgModule } from '@angular/core';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    exports: [NgbCollapseModule],
})
export class BootstrapModule {}
