import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule, MAT_BUTTON_TOGGLE_DEFAULT_OPTIONS } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { ErrorStateMatcher, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DirtyAndTouchedMatcher } from './matchers/dirty-and-touched.matcher';
import { buttonToggleConfig } from './options/button-toggle.config';
import { dialogConfig } from './options/dialog.config';
import { formFieldConfig } from './options/form-field.config';
import { snackBarConfig } from './options/snack-bar.config';

@NgModule({
    exports: [
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule, // unused
        MatDatepickerModule, // unused
        MatDialogModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule, // unused
        MatProgressSpinnerModule, // unused
        MatRippleModule, // unused
        MatSelectModule, // unused
        MatSidenavModule,
        MatTabsModule,
        MatToolbarModule,
        MatSnackBarModule,
    ],
    providers: [
        { provide: ErrorStateMatcher, useClass: DirtyAndTouchedMatcher },
        { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: dialogConfig },
        { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: formFieldConfig },
        { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: snackBarConfig },
        { provide: MAT_BUTTON_TOGGLE_DEFAULT_OPTIONS, useValue: buttonToggleConfig },
    ],
})
export class MaterialModule {}
