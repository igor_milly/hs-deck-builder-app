// Note: parts of this components' source code (including template and styles) originate from @angular/material.

import { FocusMonitor } from '@angular/cdk/a11y';
import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Inject, Input, Optional, ViewEncapsulation } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { ANIMATION_MODULE_TYPE } from '@angular/platform-browser/animations';

@Component({
    selector: 'hs-entity-button, [hs-entity-button], [hsEntityButton]',
    exportAs: 'hsEntityButton',
    inputs: ['disabled', 'disableRipple'],
    host: {
        '[attr.disabled]': 'disabled || null',
        '[class._mat-animation-noopable]': '_animationMode === "NoopAnimations"',
    },
    templateUrl: './entity-button.component.html',
    styleUrls: ['./entity-button.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityButtonComponent extends MatButton {
    @Input() contentType?: 'card' | 'tile';
    @Input() scale = 0.8;

    @HostBinding('style.transform') get transform(): string {
        return `scale(${this.scale})`;
    }

    @HostBinding('style.margin') get margin(): string {
        const fraction = (1 - this.scale) / 2;

        switch (this.contentType) {
            case 'card':
                return `${-fraction * 386}px ${-fraction * 286}px`;

            case 'tile':
                return `${-fraction * 43}px ${-fraction * 286}px`;

            default:
                return '0';
        }
    }

    get rippleClass(): string | undefined {
        return this.contentType ? `hs-${this.contentType}-button-ripple` : undefined;
    }

    constructor(el: ElementRef, monitor: FocusMonitor, @Optional() @Inject(ANIMATION_MODULE_TYPE) animationMode: string) {
        super(el, monitor, animationMode);
        if (super._getHostElement() && super._getHostElement().classList) {
            (super._getHostElement() as HTMLElement).classList.add('hs-entity-button');
        }
    }
}
