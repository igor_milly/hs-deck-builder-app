import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export class ExampleDevDialogData {
    text?: string;
}

@Component({
    selector: 'ibw-example-dev-dialog',
    templateUrl: './example-dev-dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleDevDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA) readonly data: ExampleDevDialogData) {}
}
