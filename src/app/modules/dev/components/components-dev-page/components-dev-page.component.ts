import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { timer } from 'rxjs';
import { auditTime, map } from 'rxjs/operators';
import * as ToastActions from 'src/app/modules/core/store/toast/toast.actions';
import { AppState } from 'src/app/store/app.store';
import { ExampleDevDialogComponent } from '../../components/example-dev-dialog/example-dev-dialog.component';

@Component({
    selector: 'app-components-dev-page',
    templateUrl: './components-dev-page.component.html',
    styleUrls: ['./components-dev-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentsDevPageComponent {
    readonly disabled$ = timer(0, 3000).pipe(
        map(value => value % 2),
        map(value => !!value),
    );

    readonly progress$ = timer(1000, 35).pipe(
        map(value => value % 100),
        auditTime(140),
    );

    constructor(private readonly store: Store<AppState>, private readonly dialog: MatDialog) {}

    openExampleDialog(): void {
        const dialogRef = this.dialog.open(ExampleDevDialogComponent, { data: { text: 'Example dialog beautiful content.' } });

        dialogRef.beforeClosed().subscribe(result => result === 'action' && this.store.dispatch(ToastActions.showSuccessToast({ message: `Dialog action dispatched` })));
    }

    showSuccessToast(): void {
        this.store.dispatch(ToastActions.showSuccessToast({ message: `Short success message (3 seconds)` }));
    }

    showWarningToast(): void {
        this.store.dispatch(ToastActions.showWarningToast({ message: `Short warning message (7 seconds)` }));
    }

    showErrorToast(): void {
        this.store.dispatch(ToastActions.showErrorToast({ message: `Short error message (5 seconds)` }));
    }

    showInfoToast(): void {
        this.store.dispatch(ToastActions.showInfoToast({ message: `Short info message (custom 5 seconds)`, options: { duration: 5000 } }));
    }
}
