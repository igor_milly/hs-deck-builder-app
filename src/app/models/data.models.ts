import { CardClass } from './entity.models';

export interface EntityIcons {
    base64HeroDamage: string;
    base64HeroHealth: string;
    base64HeroArmor: string;
    base64MinionDragon: string;
    base64MinionDeathrattle: string;
    base64MinionTriggered: string;
    base64MinionRace: string;
    base64MinionSilenced: string;
}

export interface EntityImages {
    cardId: string;
    base64tile: string;
    base64img256x: string;
}

export interface CardCrystals {
    cardClass: CardClass;
    base64MinionCommon: string;
    base64MinionRare: string;
    base64MinionEpic: string;
    base64MinionLegendary: string;
    base64SpellCommon: string;
    base64SpellRare: string;
    base64SpellEpic: string;
    base64SpellLegendary: string;
}

export interface EntityTemplates {
    cards: CardTemplates[];
    common: CommonTemplates;
}

export interface CardTemplates {
    cardClass: CardClass;
    base64MinionRegular: string;
    base64SpellRegular: string;
}

export interface CommonTemplates {
    base64HeroSelection: string;
    base64HeroRegular: string;
    base64HeroBoard: string;
    base64MinionBoard: string;
    base64CardSelection: string;
}
