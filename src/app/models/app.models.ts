export class DataOptions {
    constructor(readonly collectibleOnly: boolean = true, readonly cardSets?: string[], readonly cardTypes?: string[]) {}
}

// TODO rename as progress only? rename state prop as load progress?
export interface LoadingProgress {
    progress: number;
    total?: number;
}
