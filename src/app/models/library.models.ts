import { CardClass } from './entity.models';

export type CostOption = 0 | 1 | 2 | 3 | 4 | 5 | 6 | '7+';

export enum SelectionFilter {
    CLASS = 'CLASS',
    NEUTRAL = 'NEUTRAL',
    MINION = 'MINION',
    SPELL = 'SPELL',
}

export interface CardPrefilter {
    cardClass: CardClass;
    ids: string[]; // TODO rename as card-ids
    minionIdsWithNeutral: string[]; // TODO rename as minion-and-neutral-ids
    spellIdsWithNeutral: string[];
}
