import { Card, Data } from './entity.models';

export interface Deck {
    heroId: string | null;
    cardIdCounts: IdCount[];
}

export interface IdCount {
    id: string;
    count: number;
}

export interface CardCount {
    card: Data<Card>;
    count: number;
}
