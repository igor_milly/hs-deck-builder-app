import { ChangeDetectionStrategy, Component, TrackByFunction } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { asyncScheduler } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { CardClass } from 'src/app/models/entity.models';
import { risingArraySequence } from 'src/app/operators/rising-array-sequence.operator';
import { AppState } from 'src/app/store/app.store';
import { filterCollectionCards } from 'src/app/store/collection/collection.actions';
import { getCollectionCards, getCollectionFilter } from 'src/app/store/collection/collection.selectors';

@Component({
    selector: 'hs-collection-page',
    templateUrl: './collection-page.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent {
    readonly cards$ = this.store.pipe(
        select(getCollectionCards),
        concatMap(c => risingArraySequence(c, 4, 28, asyncScheduler)),
    );

    readonly filter$ = this.store.pipe(select(getCollectionFilter));

    readonly collectionFilters = Object.keys(CardClass);

    constructor(private readonly store: Store<AppState>) {}

    trackByIndex: TrackByFunction<any> = index => index;

    filterCards(filter: CardClass): void {
        this.store.dispatch(filterCollectionCards({ filter }));
    }
}
