import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'hs-root',
    templateUrl: './root.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RootComponent {
    sidebarOpened = false;
}
