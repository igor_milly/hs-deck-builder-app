import { ChangeDetectionStrategy, Component, TrackByFunction } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { asyncScheduler } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { CardCount } from 'src/app/models/deck.models';
import { Card, Data, Entity } from 'src/app/models/entity.models';
import { SelectionFilter } from 'src/app/models/library.models';
import { AppState } from 'src/app/store/app.store';
import { addCardToDeck, removeCardFromDeck, removeCardsFromDeck } from 'src/app/store/deck/deck.actions';
import { getDeckCardCounts, getDeckHero, hasDeckCards } from 'src/app/store/deck/deck.selectors';
import { filterSelectionCards } from 'src/app/store/selection/selection.actions';
import { getSelectionCards, getSelectionFilter } from 'src/app/store/selection/selection.selectors';
import { risingArraySequence } from '../../operators/rising-array-sequence.operator';

@Component({
    selector: 'hs-deck-page',
    templateUrl: './deck-page.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeckPageComponent {
    readonly selectionCards$ = this.store.pipe(
        select(getSelectionCards),
        concatMap(c => risingArraySequence(c, 4, 24, asyncScheduler)),
    );

    readonly selectionFilter$ = this.store.pipe(select(getSelectionFilter));

    readonly deckHero$ = this.store.pipe(select(getDeckHero));
    readonly deckCardCounts$ = this.store.pipe(select(getDeckCardCounts));
    readonly hasDeckCards$ = this.store.pipe(select(hasDeckCards));

    readonly selectionFilters = Object.keys(SelectionFilter);

    constructor(private readonly store: Store<AppState>) {}

    trackByEntityId: TrackByFunction<Data<Entity>> = (_, { entity }) => entity.id;

    trackByCardId: TrackByFunction<CardCount> = (_, { card }) => card.entity.id;

    filterCards(filter: SelectionFilter): void {
        this.store.dispatch(filterSelectionCards({ filter }));
    }

    addCard(card: Card): void {
        this.store.dispatch(addCardToDeck({ card }));
    }

    removeCard(id: string): void {
        this.store.dispatch(removeCardFromDeck({ id }));
    }

    removeAllCards(): void {
        this.store.dispatch(removeCardsFromDeck());
    }
}
