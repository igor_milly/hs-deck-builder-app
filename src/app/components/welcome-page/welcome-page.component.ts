import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'hs-welcome-page',
    templateUrl: './welcome-page.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WelcomePageComponent {}
