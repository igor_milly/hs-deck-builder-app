# TODO List

This is a quick snippet of what I intend to work on in the near future. It is easy to read and replaces proper project management tools.

## High Priority Tasks

-   normalize store files and logic
-   migrate template + icon data
-   loading progress store
-   finalize all current store logic

## Priority Tasks

-   rename and solidify entity components
-   responsive layout skeleton
-   complex resolvers
-   include jest unit testing suite

## Features

-   toolbar
-   sidebar
-   reboot
-   typography
-   helper classes
-   landing screen
-   responsive library layout
-   shadow dom and encapsulated styles for regular components
-   hero view + selection
-   app icon, multiple sizes
-   typography config
-   waved entity names
-   error handling

## Known bugs / issues

-   mat ripple positioning on scaled elements
-   active tab link not set active on load
-   entity blur on change detection
