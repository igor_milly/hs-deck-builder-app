# HS Deck Builder App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) before June 22nd, 2019.

## Features

Of course, the development is in progress all around... check out the [current TODO list](TODO.md) for features to come.

### Data Mining

Card image data can be mined using the [image mining tool](src/tools).

### Cards

Available card types for view: 'MINION', 'SPELL'

### Deck Builder

Create your own deck! Add / Remove Cards, filter your available cards by its class or type and have a satisfying overview of your current deck!

Missing features: responsive layout, hero selection (defaults to 'PRIEST'), charts,...

### Collection

Browse your available cards without restrictions! All class filters are available!

Missing features: responsive layout

### Examples

Advanced custom Redux / NgRx structure for easy code maintenance. See [Root Store](src/app/store) for reference.

On each raw data load - extract card ids by category for faster filtering experience. See [Prefilter Library Feature Store](src/app/store/prefilter) for reference.

## Getting Started

Install [Node.js](https://nodejs.org/en/) and run `npm ci&&npm start`.

### IDE Extensions

For development, an IDE such as [Visual Studio Code](https://code.visualstudio.com) with [Typescript](https://www.typescriptlang.org) capabilities is required, alongside with the following plugins / extensions:

-   [Angular](https://angular.io/) capabilities: [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)
-   code linting: [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)
-   code formatting: [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - please set auto-format on save within your IDE
-   git capabilities: [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)

<sup>Shared extension recommentations for Visual Studio Code are already included within the repository.</sup>

### IDE Settings

For consistent development and to fully enable the power of some plugins / extensions, please configure your IDE as follows (based on Visual Studio Code):

```json
{
    "editor.tabSize": 4,
    "editor.detectIndentation": false,
    "editor.formatOnSave": true,
    "editor.codeActionsOnSave": {
        "source.fixAll": true,
        "source.organizeImports": true
    },
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "extensions.ignoreRecommendations": false,
    "angular.experimental-ivy": true
}
```

<sup>Shared workspace settings for Visual Studio Code are already included within the repository.</sup>

## Styling Setup

### Normalize

Visit [normalize.css](https://github.com/necolas/normalize.css/), [Bootstrap Reboot](https://getbootstrap.com/docs/5.1/content/reboot/) and [Custom Base](/src/styles/scss/common/_base.scss) for normalize reference.

### Typography

Visit [Bootstrap Typography](https://getbootstrap.com/docs/5.1/content/reboot/), [Custom Typography](/src/styles/scss/common/_typography.scss) and [Imported Fonts](/src/styles/fonts/HKGrotesk/HKGroteskLegacy.css) for typography reference.

<!-- ### Icons

Visit [Font Awesome 4.7.0](https://fontawesome.com/v4.7.0/icons/) for icon reference. -->

### Grid

Visit [Bootstrap Grid](https://getbootstrap.com/docs/5.1/layout/grid/) <!-- and [Custom Container Styles](/src/styles/scss/common/_container.scss)  -->for grid utilities reference.

### Utilities

Visit [Bootstrap Utilities](https://getbootstrap.com/docs/5.1/utilities/spacing/) and [Bootstrap Helpers](https://getbootstrap.com/docs/5.1/helpers/visually-hidden/) for style utilities reference. For local filters and changes, check out [Custom Utilities](/src/styles/scss/bootstrap/_utilities.scss), [Custom Bootstrap Variables](/src/styles/scss/bootstrap/_variables_.scss) and [Custom Styles](/src/styles/scss/styles.scss) in general.

## Development server

Run `ng serve` / `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component folder-name/component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` / `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. The `--prod` / `--configuration=production` flag is not required for a production build anymore as it is now default.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
