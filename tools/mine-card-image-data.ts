import fs from 'fs';
import fetch from 'node-fetch';
import path from 'path';
import { forkJoin, from, Observable } from 'rxjs';
import { concatMap, reduce } from 'rxjs/operators';
import { Entity } from '../src/app/store/entity';
import { EntityImageData } from '../src/app/store/image';
import { cardSets, collectibleOnly } from '../src/config';

console.log('Initializing image miner...');
console.log();

import(`../src/assets/data/hearthstonejson/v1/31353/enUS/cards${collectibleOnly ? '.collectible' : ''}.json`)
    .then(data => (data.default as unknown) as Entity[])
    .then(items => items.filter(card => cardSets.some(set => card.set === set)))
    .then(cards => {
        const cardsTotal = cards.length;

        const startTime = new Date().valueOf();

        console.log('Total cards to process: ' + cardsTotal);
        console.log();

        from(cards.filter(card => cardSets.some(set => card.set === set)).map(card => card.id))
            .pipe(
                concatMap<string, EntityImageData, Observable<[string, string]>>(
                    id =>
                        forkJoin(
                            fetch(`https://art.hearthstonejson.com/v1/tiles/${id}.jpg`)
                                .then(res => res.buffer())
                                .then(buffer => 'data:image/jpeg;base64,' + buffer.toString('base64')),
                            fetch(`https://art.hearthstonejson.com/v1/256x/${id}.jpg`)
                                .then(res => res.buffer())
                                .then(buffer => 'data:image/jpeg;base64,' + buffer.toString('base64')),
                        ),
                    (id, [base64tile, base64img256x], index) => {
                        console.log(`Downloading: ${id} (${index + 1} / ${cardsTotal})`);
                        return { cardId: id, base64tile, base64img256x };
                    },
                ),
                reduce<EntityImageData, EntityImageData[]>((acc, value) => [...acc, value], []),
            )
            .subscribe(
                data =>
                    fs.writeFileSync(
                        path.join('src', 'assets', 'data', 'hearthstonejson', 'mined', 'card-image-data.json'),
                        JSON.stringify(data),
                    ),
                err => console.error(err),
                () => {
                    const time = new Date().valueOf() - startTime;

                    const minutes = Math.floor(time / 1000 / 60);
                    const seconds = Math.round(time / 1000) - minutes * 60;

                    console.log();
                    console.log(`Mining finished in ${minutes}:${seconds} minutes.`);
                },
            );
    });
